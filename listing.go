package tradetracker

// CSV HEADERS: "product ID";name;currency;price;description;productURL;imageURL;categories;brand;categoryPath;deliveryCosts;deliveryTime;EAN;stock;subcategories;specialOffer;fromPrice;promo_active;campaignID

type Listing struct {
	EAN           int
	Name          string
	Brand         string
	Currency      string
	Price         float64
	FromPrice     float64
	DeliveryCosts float64
	Stock         int
	Description   string
	URL           string
	Image         string
	Category      string
	Category2     string
	Category3     string
}
