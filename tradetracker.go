package tradetracker

import (
	"errors"
	"strconv"

	"gitlab.com/bazzz/csv"
	"gitlab.com/bazzz/textparse"
)

type Client struct {
	affiliateID int
}

func New(affilateID int) *Client {
	t := Client{affiliateID: affilateID}
	return &t
}

func (t *Client) GetFeed(feedID int) ([]Listing, error) {
	if feedID <= 0 {
		return nil, errors.New("could not get feed: feedID must be 1 or higher")
	}
	url := "https://pf.tradetracker.net/?aid=" + strconv.Itoa(t.affiliateID) + "&encoding=utf-8&type=csv&fid=" + strconv.Itoa(feedID) + "&categoryType=2&additionalType=2&csvDelimiter=%3B&csvEnclosure=%22&filter_extended=1"
	doc, err := csv.New(url, ';', "")
	if err != nil {
		return nil, err
	}
	listings := make([]Listing, 0)
	for _, line := range doc.Rows() {
		listing := Listing{
			EAN:           textparse.Integer(line.Get("EAN")),
			Name:          textparse.Clean(line.Get("name")),
			Brand:         line.Get("brand"),
			Currency:      line.Get("currency"),
			Price:         textparse.Float(line.Get("price")),
			FromPrice:     textparse.Float(line.Get("fromPrice")),
			DeliveryCosts: textparse.Float(line.Get("deliveryCosts")),
			Stock:         textparse.Integer(line.Get("stock")),
			Description:   textparse.Clean(line.Get("description")),
			URL:           line.Get("productURL"),
			Image:         line.Get("imageURL"),
			Category:      line.Get("categories"),
			Category2:     line.Get("categoryPath"),
			Category3:     line.Get("subcategories"),
		}
		listings = append(listings, listing)
	}
	return listings, nil
}
