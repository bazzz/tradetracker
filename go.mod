module gitlab.com/bazzz/tradetracker

go 1.20

require (
	gitlab.com/bazzz/csv v0.0.0-20240331093736-2b723a579d10
	gitlab.com/bazzz/textparse v0.0.0-20240331144504-cd8fac84c8d8
)
